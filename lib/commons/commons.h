#ifndef COMMONS_H_
#define COMMONS_H_



/*
If the version have the external CAN MCP3208 uncomment the next line
 */
#define CAN_SPI

#ifdef CAN_SPI

static const uint8_t SPI_CS = 7;        // SPI slave select
#include "ADC_SPI.h"
extern ADC_SPI adc;

/*
    ADC pin definitions
 */
#define PIN_UAC     ADC::SINGLE_0
#define PIN_I1      ADC::SINGLE_1
#define PIN_I2      ADC::SINGLE_2
#define PIN_I3      ADC::SINGLE_3
#define PIN_I4      ADC::SINGLE_4
#define PIN_2_5     ADC::SINGLE_5
#define PIN_UDIR    ADC::SINGLE_6
#define PIN_UDC     ADC::SINGLE_7

#else

const int ADC_COUNTS = 1024;
const int ADC_COUNTS_HIGH = ADC_COUNTS*0.55;
const int ADC_COUNTS_LOW = ADC_COUNTS*0.45;

#endif

#endif /*COMMONS_H_ */
