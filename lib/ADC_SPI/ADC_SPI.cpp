
#include "ADC_SPI.h"

ADC_SPI::ADC_SPI(uint8_t csPin, SPIClass *spi)
: mCsPin(csPin)
, mSplSpeed(0)
, mSpi(spi) {}

ADC_SPI::ADC_SPI(uint8_t csPin)
: ADC_SPI(csPin, &SPI) {}

void ADC_SPI::init(){

    pinMode(mCsPin, OUTPUT);		// set spi cs as an output pin
    digitalWrite(mCsPin, HIGH);     // idle is level '1'

    // initialize SPI interface for MCP3208
    SPISettings settings(ADC_CLK, MSBFIRST, SPI_MODE0);
    SPI.begin();
    SPI.beginTransaction(settings);

    getVcc();
}


uint16_t ADC_SPI::read(Channel ch) const
{
    // transfer spi data
    return transfer(createCmd(ch));
}

/*
auto set Vcc
 */
float ADC_SPI::getVcc(){
    mVref = read(SINGLE_5);
    mVcc = 2.5*4096;            // theorical voltage reference  multiplied by resolution
    mVcc /= mVref;              // divided by real voltage reference

    return mVcc;
}

ADC_SPI::SpiData ADC_SPI::createCmd(Channel ch)
{
  // base command structure
  // 0b000001cccc000000
  // c: channel config
  return {
    // add channel to basic command structure
    .value = static_cast<uint16_t>((0x0400 | (ch << 6)))
  };
}

uint16_t ADC_SPI::transfer(SpiData cmd) const
{
  SpiData adc;

  // activate ADC with chip select
  digitalWrite(mCsPin, LOW);

  // send first command byte
  mSpi->transfer(cmd.hiByte);
  // send second command byte and receive first(msb) 4 bits
  adc.hiByte = mSpi->transfer(cmd.loByte) & 0x0F;
  // receive last(lsb) 8 bits
  adc.loByte = mSpi->transfer(0x00);

  // deactivate ADC with slave select
  digitalWrite(mCsPin, HIGH);

  return adc.value;
}
